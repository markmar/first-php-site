<!Doctype HTML>
<html>
<head>
 
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="style.css">
    <meta charset="utf-8">
    <link rel="stylesheet" href="font/css/font-awesome.css">
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script
			  src="https://code.jquery.com/jquery-3.4.1.slim.js"
			  integrity="sha256-BTlTdQO9/fascB1drekrDVkaKd9PkwBymMlHOiG+qLI="
			  crossorigin="anonymous"></script>
</head>
<body>

    <header>
        
          <div >
              <img src="img/logo.png" alt="логотип">
          </div>
          
          <div>
                 <a href="auth.php"><span><i class="fa fa-lock fa-3x autoriz"></i></span></a>
          </div>
    </header>