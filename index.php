<?php include("includes/header.php"); ?>
          <div class="container ">
       
        <div class="left-menu ">
            <div class="navigation ">
                    <a href="index.php">
                        <div class="addline current">Обо мне</div>
                    </a>
                    <a href="my-work.php">
                        <div class="addline">Мои работы</div>
                    </a>
                    <a href="call-me.php">
                        <div>Связаться со мной</div>
                    </a>
            </div>
            <?php include("includes/contacts.php"); ?>
        </div>
      
        <div class="content ">
            <div class="main-information">

                <div class="main-name">Основная информация</div>
                <div class="main-content">
                <div class="foto">
                    <img src="img/foto.png" alt="">
                </div>
                <div class="information">
                        <div><span>Меня зовут:</span>   Косов Михаил Викторович</div>
                        <div><span>Мой возраст:</span>   19 лет</div>
                        <div><span>Мой город:</span>   Белгород, Росссия</div>
                        <div><span>Моя специализация:</span>  FRONTEND разработчик</div>
                        <div><span>Ключевые навыки:</span><div class="tech">html </div><div class="tech"> css </div><div class = "tech"> javascript </div><div class="tech"> php </div>
                        </div></div>
                </div>

            </div>
            <div class="main-information">
                <div class="main-name">Образование</div>
                
                <div class="main-content">
                <span><i class="fa fa-graduation-cap fa-4x fa-edu"></i></span>
                  <div class="information">
                      <div><span>Колледж:</span> Белгородский Университет Кооперации Экономики и Права</div>
                      <div><span>Факультет:</span> Прикладная информатика</div>
                      <div><span>Год окончания обучения:</span> 2018</div>
                  </div>
                  </div>
                  
                  <div class="main-content">
                <span><i class="fa fa-youtube fa-4x fa-edu" ></i></span>
                  <div class="information">
                    <div>Самостоятельное изучение языков в общедоступных источниках</div>
                  </div>
                  </div>
            </div>
        </div>
        
    </div>
    
<?php include("includes/footer.php"); ?>