var numberOfCorrectAnswer = 0,
allAnswers = [],
numberOfQuestion = 0, 
secondsOfTest = 0;
$("#start").click(
    function() {
        $("#start").css("display", "none");
        $("#list").css("display", "block");
        timer();
        startTest();
});

function timer(){
    setInterval(tick, 1000);
    function tick() {
     secondsOfTest++;
    }
}

function startTest() {
    $("#list").css("display", "block");
    $(".result").css("display", "none");
    outputQuestions(); 
    outputAnswers();
    monitor(); 
}

function outputQuestions () { 
    var questions = ["2+4", "10-34", "140/70", "12*9", "389+781", "456/3", "998-1036", "58*12", "742-479", "63*13"]; 
    $(".task").text("Сколько будет " + questions[numberOfQuestion]);
}

function outputAnswers () {
    allVersionsAnswers = {
0 : {
    answer1: 6,
    answer2: 4,
    answer3: 2,
    answer4: 8},
 1 : {
    answer1: 34,
    answer2: 24,
    answer3: -24,
    answer4: -34},
 2 : {
    answer1: 1,
    answer2: 2,
    answer3: 3,
    answer4: 4},
 3 : {
    answer1: 112,
    answer2: 118,
    answer3: 98,
    answer4: 108},
 4 : {
    answer1: 1270,
    answer2: 1080,
    answer3: 1070,
    answer4: 1170},
 5 : {
    answer1: 152,
    answer2: 153,
    answer3: 151,
    answer4: 154},
 6 : {
    answer1: -38,
    answer2: 38,
    answer3: 83,
    answer4: -83},
 7 : {
    answer1: 666,
    answer2: 966,
    answer3: 696,
    answer4: 699},
 8 : {
    answer1: 233,
    answer2: 263,
    answer3: 623,
    answer4: 323},
 9 : {
    answer1: 919,
    answer2: 819,
    answer3: 619,
    answer4: 719}};
    $(".answer1").text(allVersionsAnswers[numberOfQuestion].answer1);
    $(".answer2").text(allVersionsAnswers[numberOfQuestion].answer2);
    $(".answer3").text(allVersionsAnswers[numberOfQuestion].answer3);
    $(".answer4").text(allVersionsAnswers[numberOfQuestion].answer4);
    unchecked();
}

function unchecked(){
    $("input").prop("checked", false);
}

function monitor() { 
    $("input").click(
        function() {
            var clickId = $(this).attr("id");
            allAnswers.push(clickId);
            numberOfQuestion++;
            nextQuest();}
)}

function nextQuest (){
    if (allAnswers.length == 10){
        endTest ()
    }
    else{
        correctAnswer();
        outputQuestions();  
        outputAnswers(); 
    }
}

function correctAnswer(){
    var correctKey = ["answer1", "answer3", "answer2", "answer4", "answer4", "answer1", "answer1", "answer3", "answer2", "answer2"];
    if (allAnswers[numberOfQuestion-1] == correctKey[numberOfQuestion-1]){
        numberOfCorrectAnswer++;
    }
    $("#correctAnswer").text(numberOfCorrectAnswer);
}

function endTest (){
    $(".task").text("Поздравляю! Вы прошли тест");
    $("#list").css("display", "none");
    $(".result").css("display", "block");
    $(".result").css("margin-left", "44px");
    $(".result").css("margin-right", "44px");
    $("button").css("margin", "7px auto");
    $("#time").text(secondsOfTest);
    $("#unCorrectAnswer").text(10-numberOfCorrectAnswer);
}

$("#reboot").click(
    function reset(){
    numberOfQuestion = 0;
    secondsOfTest = 0;
    numberOfCorrectAnswer = 0;
    $("#correctAnswer").text(numberOfCorrectAnswer);
    allAnswers = [];
    $("#list").css("display", "block");
    $(".result").css("display", "none");
    outputQuestions();
    outputAnswers();
}
)


