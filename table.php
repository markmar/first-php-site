<?php session_start();

if(!isset($_SESSION["session_username"])):
header("location:auth.php");
else:
?>
<?php require_once("includes/connection.php"); ?>

<!Doctype HTML>
<html>
<head>
 
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="style.css">
    <meta charset="utf-8">
    <link rel="stylesheet" href="font/css/font-awesome.css">
    <script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<body>
<header>
        
          <div >
              <img src="img/logo.png" alt="логотип">
          </div>
          <div id="welcome">
            <h2>Добро пожаловать, <span><?php echo $_SESSION['session_username'];?>! </span></h2>
            <p><a href="logout.php">Выйти</a> из системы</p>
          </div>
          <div>
                 <a href="index.php"><span><i class="fa fa-lock fa-3x autoriz"></i></span></a>
          </div>
    </header>
<?php $result = mysqli_query ($con, "SELECT * FROM invite");
    if ($result){
        $rows = mysqli_num_rows($result);
        echo "<table><tr><th>Id</th><th>Имя</th><th>Email</th><th>Сообщение</th></tr>";
        for ($i = 0 ; $i < $rows ; ++$i){
            $row = mysqli_fetch_row($result);
                echo "<tr>";
            for ($j = 0 ; $j < 4 ; ++$j) echo "<td>$row[$j]</td>";
                echo "</tr>";
        }
        echo "</table>";
        mysqli_free_result($result);
    }
?>
	
<?php include("includes/footer.php"); ?>
	
<?php endif; ?>